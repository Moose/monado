Add a whole bunch of structs and functions for all of the different layers
in OpenXR. The depth layer information only applies to the stereo projection
so make a special stereo projection with depth layer.
