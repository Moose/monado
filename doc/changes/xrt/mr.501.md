Add const to all compositor arguments that are info structs, making the interface safer and
more clear. Also add `max_layers` field to the `xrt_compositor_info` struct.
