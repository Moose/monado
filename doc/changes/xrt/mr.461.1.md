Remove the `flip_y` parameter to the creation of the native compositor, this
is now a per layer thing.
