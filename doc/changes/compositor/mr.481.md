compositor: Check the protected content bit, and return a non-success code if it's set. Supporting this is optional in OpenXR, but lack of support must be reported to the application.
