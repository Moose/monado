psmv: Introduce proper grip and aim poses, correctly rotate the grip pose to
follow the spec more closely. The aim poses replaces the previous ball tip pose
that was used before for aim.
