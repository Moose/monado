Allow building some components without Vulkan. Vulkan is still required for the compositor and therefore the OpenXR runtime target.
