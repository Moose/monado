OpenXR: Implement the function `xrEnumerateBoundSourcesForAction`, currently we
only bind one input per top level user path and it's easy to track this.
