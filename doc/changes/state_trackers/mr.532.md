OpenXR: More correctly implement `xrGetInputSourceLocalizedName` allowing apps
to more accurently tell the user which input to use.
