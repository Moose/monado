OpenXR: Make the `xrGetCurrentInteractionProfile` conformance tests pass, needed
to implement better error checking as well as generating
`XrEventDataInteractionProfileChanged` events to the client.
