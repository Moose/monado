OpenXR: Be sure to return `XR_ERROR_FEATURE_UNSUPPORTED` if the protected content bit is set and the compositor does not support it.
